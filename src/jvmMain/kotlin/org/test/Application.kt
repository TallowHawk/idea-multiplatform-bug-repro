package org.test

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.routing
import org.slf4j.event.Level

fun Application.main() {
    install(CallLogging) {
        level = Level.INFO
    }

    routing {
        get("/hello/{world}") {
            val param = call.parameters["world"]!!

            call.respond(param)
        }
    }
}